﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using StreetHawkCrossplatform;
using System.Collections.ObjectModel;

namespace XamHawkDemo
{
	public partial class FeedsPage : ContentPage
	{

        public FeedsPage()
		{
			InitializeComponent();
		}

       public static ObservableCollection<Feeds> FeedList = new ObservableCollection<Feeds>();

        public class Feeds
        {
            public string DisplayName { get; set; }
        }

        protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<IStreetHawkAnalytics>().NotifyViewEnter(this.GetType().Name);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			DependencyService.Get<IStreetHawkAnalytics>().NotifyViewExit(this.GetType().Name);
		}

		private void buttonFetchFeedClick(object sender, EventArgs e)
		{
            if(FeedList.Count > 10)
            {
                FeedList.Clear();
            }
           
            FeedList.Add(new Feeds{ DisplayName = DependencyService.Get<IStreetHawkFeeds>().FeedListener()});
            EmployeeView.ItemsSource = FeedList;
		}
	}
}

