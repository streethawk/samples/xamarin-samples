﻿using System;
using StreetHawkCrossplatform;
using Com.Streethawk.Library.Geofence;
using Android.App;
using Android.Util;
using System.Collections.Generic;

using XamHawkDemo.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(StreetHawkGeofence))]
namespace XamHawkDemo.Droid
{
	public class StreetHawkGeofence :IStreetHawkGeofence,INotifyGeofenceTransition
	{
		private static Application mApplication;


		public StreetHawkGeofence() { }

		public StreetHawkGeofence(Application application)
		{
			mApplication = application;
		}

		public bool GetIsDefaultLocationServiceEnabled()
		{
			Log.Error("StreetHawk", "Function GetIsDefaultLocationServiceEnabled is not implemented for Android");
			return false;
		}

		public bool GetIsLocationServiceEnabled()
		{
			Log.Error("StreetHawk", "Function GetIsDefaultLocationServiceEnabled is not implemented for Android");
			return false;
		}


		private static RegisterForGeofenceCallback mGeofenceCallBack;

		public IntPtr Handle
		{
			get;
			set;
		}

		public void RegisterForGeofenceStatus(RegisterForGeofenceCallback cb)
		{
			mGeofenceCallBack = cb;
		}

		public void SetIsDefaultLocationServiceEnabled(bool isEnable)
		{
			Log.Error("StreetHawk", "Function GetIsDefaultLocationServiceEnabled is not implemented for Android");
		}

		public void SetIsLocationServiceEnabled(bool isEnable)
		{
			Log.Error("StreetHawk", "Function GetIsDefaultLocationServiceEnabled is not implemented for Android");
		}

		public void StartGeofenceMonitoring()
		{
			SHGeofence.GetInstance(mApplication.ApplicationContext).StartGeofenceMonitoring();
		}

		public void StartGeofenceWithPermissionDialog(string message)
		{
			Log.Error("StreetHawk", "Function StartGeofenceWithPermissionDialog is not implemented for Android");
		}

		public void StopGeofenceMonitoring()
		{
			SHGeofence.GetInstance(mApplication.ApplicationContext).StopMonitoring();
		}

		public void OnDeviceEnteringGeofence()
		{
			//TODO Add function in native to support this callback

			Log.Error("StreetHawk","OnDeviceEnteringGeofence is not supported in this release");
		}

		public void OnDeviceLeavingGeofence()
		{
			//TODO Add function in native to support this callback

			Log.Error("StreetHawk", "OnDeviceEnteringGeofence is not supported in this release");
		}

		public void Dispose()
		{
			
		}

        /* GeoFance Data p0 consists of:
         * Geofence ID  = getGeofenceID
         * Latitude     = getLatitude
         * Longitude  = getLongitude
         * Radius = getRadius
         * ParentID  = getParentID
         * Distance = getDistance
         */
        public void OnDeviceEnteringGeofence(IList<GeofenceData> p0)
        {
            // When Device enters GeoFence
            foreach (var item in p0)
            {
                String geoID = item.GeofenceID;
                double geoLatitude = item.Latitude;
                double geoLongitude = item.Longitude;
                float georadius = item.Radius;
                double geoDistance = item.Distance;
            }
            return;
        }

        public void OnDeviceLeavingGeofence(IList<GeofenceData> p0)
        {
            // When Device Leaves GeoFence
            foreach (var item in p0)
            {
                String geoID = item.GeofenceID;
                double geoLatitude = item.Latitude;
                double geoLongitude = item.Longitude;
                float georadius = item.Radius;
                double geoDistance = item.Distance;
            }
            return;
        }
    }
}

