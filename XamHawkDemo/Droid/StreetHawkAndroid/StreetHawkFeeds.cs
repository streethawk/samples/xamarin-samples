﻿using System;
using StreetHawkCrossplatform;
using Com.Streethawk.Library.Feeds;
using Android.App;
using Org.Json;
using System.Collections.Generic;
using Android.Util;

using Android.Content;
using XamHawkDemo.Droid;
using Android.Runtime;
using Java.Util;

[assembly: Xamarin.Forms.Dependency(typeof(StreetHawkFeeds))]
namespace XamHawkDemo.Droid
                     
{
	public class StreetHawkFeeds : IStreetHawkFeeds,ISHFeedItemObserver
	{
		private static Application mApplication;

		private const string FEED_ID = "feed_id";

		private const string TITLE = "title";

		private const string MESSAGE = "message";

		private const string CAMPAIGN = "campaign";

		private const string CONTENT = "content";

		private const string ACTIVATES = "activates";

		private const string EXPIRES = "expires";

		private const string CREATED = "created";

		private const string MODIFIED = "modified";

		private const string DELETED = "deleted";

        private static MainActivity main;

        public static JSONObject Feedlist = null;
        

        public StreetHawkFeeds() { }

		public StreetHawkFeeds(Application application, MainActivity activity)
		{
			mApplication = application;
            main = activity;
        }
        
        public void Register()
        {
            Intent serviceToStart = new Intent(main, typeof(FeedService));
            main.StartService(serviceToStart);
        }

        public String FeedListener()
        {
            // Call ReadFeedData to get the latest Feed from Streethawk Server
            SHFeedItem.GetInstance(main).ReadFeedData(0);

            if (Feedlist != null)
            {
                var arrayFeeds = new List<SHFeedObject>();

                if (Feedlist.Length() > 0)
                {
                    if (Feedlist.Has("error"))
                    {
                        return Feedlist.GetString("error");
                    }

                    var feeds = Feedlist.GetJSONArray("results");
                    for (int i = 0; i < feeds.Length(); i++)
                    {
                        try
                        {
                            JSONObject jsonObj = feeds.GetJSONObject(i);
                            SHFeedObject obj = new SHFeedObject();

                            if (jsonObj.Has(FEED_ID))
                                obj.feed_id = jsonObj.GetString(FEED_ID);
                            var contentObj = jsonObj.GetJSONObject(CONTENT);
                            return contentObj.GetString("aps");
                        }
                        catch (JSONException e)
                        {
                            e.PrintStackTrace();

                            throw;
                        }
                    }
                }
            }
            return "No feeds at the moment";
        }
       
        public void NotifyFeedResult(string feedid, int result)
		{
			SHFeedItem.GetInstance(mApplication.ApplicationContext).NotifyFeedResult(feedid,result);
		}

		public void NotifyFeedResult(string feedid, string stepid, string feedresult, bool feedDelete, bool completed)
		{
            String getfeedid = feedid;
            String getstepid = stepid;
            String getresult = feedresult;
            Boolean getfeeddel = feedDelete;
            Boolean getcomplete = completed;
        }

		private static RegisterForNewFeedCallback mNewFeedCallbak;

		public IntPtr Handle
		{
			get;
			set;
		}

        //IntPtr IJavaObject.Handle => throw new NotImplementedException();

        public void OnNewFeedAvailableCallback(RegisterForNewFeedCallback cb)
		{
			//TODO : implement broadcast receiver in native to receive feed notification and notifys server
			Log.Error("StreetHawk", "OnNewFeedAvailableCallback is not available for this relase");

		}

		private static RegisterForFeedCallback mRegisterForFeedCallBack;
		public void ReadFeedData(int offset, RegisterForFeedCallback cb)
		{
			mRegisterForFeedCallBack = cb;
			SHFeedItem.GetInstance(mApplication.ApplicationContext).ReadFeedData(offset);
		}

		public void SendFeedAck(string feedid)
		{
			SHFeedItem.GetInstance(mApplication.ApplicationContext).SendFeedAck(feedid);
		}

		public void ShFeedReceived(JSONArray feeds)
		{
			if(null!=mRegisterForFeedCallBack){
				if (null != feeds)
				{
					List<SHFeedObject> arrayFeeds = new List<SHFeedObject>();
					for (int i = 0; i < feeds.Length(); i++)
					{
						try
						{
							JSONObject jsonObj = feeds.GetJSONObject(i);
							SHFeedObject obj = new SHFeedObject();
							obj.feed_id = jsonObj.GetString(FEED_ID);
							obj.title = jsonObj.GetString(TITLE);
							obj.message = jsonObj.GetString(MESSAGE);
							obj.campaign = jsonObj.GetString(CAMPAIGN);
							obj.content = jsonObj.GetString(CONTENT);
							obj.activates = jsonObj.GetString(ACTIVATES);
							obj.expires = jsonObj.GetString(EXPIRES);
							obj.created = jsonObj.GetString(CREATED);
							obj.modified = jsonObj.GetString(MODIFIED);
							obj.deleted = jsonObj.GetString(DELETED);
							arrayFeeds.Add(obj);

						}
						catch (JSONException e)
						{
							e.PrintStackTrace();
						}
					}
					mRegisterForFeedCallBack.Invoke(arrayFeeds,null);
				}
			}
		}
		public void Dispose()
		{
			
		}

        void ISHFeedItemObserver.SHNotifyNewFeedItem()
        {
            return;
        }

        void ISHFeedItemObserver.ShFeedReceived(JSONObject p0)
        {
            return;
        }

        void IDisposable.Dispose()
        {
            return;
        }

    }
}