﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using StreetHawkCrossplatform;

namespace XamHawkDemo
{
	public partial class InstallInfoPage : ContentPage
	{
		public InstallInfoPage()
		{
			InitializeComponent();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<IStreetHawkAnalytics>().NotifyViewEnter(this.GetType().Name);

			this.labelAppKey.Text = string.Format("App key: {0}", DependencyService.Get<IStreetHawkAnalytics>().GetAppKey());
			this.labelInstallId.Text = string.Format("Install id: {0}", DependencyService.Get<IStreetHawkAnalytics>().GetInstallId());
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			DependencyService.Get<IStreetHawkAnalytics>().NotifyViewExit(this.GetType().Name);
		}
	}
}

