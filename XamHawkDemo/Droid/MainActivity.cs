﻿using Android.App;
using Android.Content.PM;
using Android.Widget;
using Android.OS;
using XamHawkDemo.Droid;
using Com.Streethawk.Library.Push;
using Android.Content;

[assembly: Xamarin.Forms.Dependency(typeof(MainActivity))]
namespace XamHawkDemo.Droid
{
	[Activity(Label = "XamHawkDemo.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity 
    {
       

        protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;
			base.OnCreate(bundle);
			global::Xamarin.Forms.Forms.Init(this, bundle);
			LoadApplication(new App());

			new StreetHawkAnalytics(Application);
			new StreetHawkGrowth(this);
            new StreetHawkPush(Application, this);
            new StreetHawkFeeds(Application, this);
			new StreetHawkLocation(Application);
			new StreetHawkGeofence(Application);
			new StreetHawkBeacons(Application);
        }
	}
}

