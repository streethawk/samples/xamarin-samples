﻿using Xamarin.Forms;
using System.Threading.Tasks;

using StreetHawkCrossplatform;

namespace XamHawkDemo
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new XamHawkDemoPage())
			{
				BarBackgroundColor = Color.FromRgb(101, 46, 145),
				BarTextColor = Color.White,
			};
		}

		static Task<bool> SetupApp(NavigationPage navpage)
		{
			var tcs = new TaskCompletionSource<bool>();
			StartPage startPage = new StartPage();
			startPage.taskCompletionSource = tcs;
			navpage.Navigation.PushModalAsync(startPage);
			return tcs.Task;
		}

		protected override async void OnStart()
		{
			// Handle when your app starts

			// Check whether need to setup app key and GCM.

			if (string.IsNullOrEmpty(DependencyService.Get<IStreetHawkAnalytics>().GetAppKey()))
			{
				await SetupApp((NavigationPage)this.MainPage);
			}

			//Optional: enable XCode console logs.
			DependencyService.Get<IStreetHawkAnalytics>().SetEnableLogs(true);
          
			// Initialize StreetHawk when App starts.
			//Mandatory: set app key and call init.
			DependencyService.Get<IStreetHawkAnalytics>().Init();

            // Call this to start the JSON Service
            DependencyService.Get<IStreetHawkPush>().Register();

            // Call this to start the Feed Listner Service
            DependencyService.Get<IStreetHawkFeeds>().Register();
            
            //Optional: callback when install register successfully.
            DependencyService.Get<IStreetHawkAnalytics>().RegisterForInstallEvent(delegate (string installId)
			{
				Device.BeginInvokeOnMainThread(() =>
					{
						MainPage.DisplayAlert("Install register successfully: ", installId, "OK");
					});
			});
            
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

