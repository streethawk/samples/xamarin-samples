App demonstrates StreetHawk Xamarin component in Xamarin.Forms app

Reminder Checks:      
- JsonService.cs in Droid is in the project, if not, add into Droid project under "XamHawkDemo\Droid"    
- have the latest versions of Xamarin Forms, Support and google Cloud messaging.    
- have the latest StreetHawk version 1.8.8, if not either use Manage Nuget Package   https://www.nuget.org/packages/StreethawkPush/ or enter in command ```Install-Package StreethawkPush```    

Note: 
If you get java exit code 2 error then make sure to enable Multi-dex.  

using ```<AndroidEnableMultipleDex>true</AndroidEnableMultipleDex>```  
or  ```<AndroidEnableMultiDex>true</AndroidEnableMultiDex>```