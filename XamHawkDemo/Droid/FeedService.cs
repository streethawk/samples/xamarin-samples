﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Streethawk.Library.Feeds;
using Org.Json;
using Android.Util;

namespace XamHawkDemo.Droid
{
    [Service]
    class FeedService : Service, ISHFeedItemObserver
    {
        static readonly string TAG = "streethawk Service";
        bool isStarted;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {    // Register the StreetHawk Feed Observer to get notified of when users recieves a Feed. 
            SHFeedItem.GetInstance(this).RegisterFeedItemObserver(this);
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (isStarted)
            {
                Log.Info(TAG, "OnStartCommand: This service has already been started.");
            }
            else
            {
                Log.Info(TAG, "OnStartCommand: The service is starting.");
                isStarted = true;
            }

            // This tells Android to restart the service if it is killed to reclaim resources.
            return StartCommandResult.Sticky;
        }


        public void ShFeedReceived(JSONObject feedResults)
        {
            if (feedResults != null)
            {
                if (feedResults.Length() > 0)
                {
                    StreetHawkFeeds.Feedlist = feedResults;
                }
            }
        }

        public void SHNotifyNewFeedItem()
        {
           
        }
    }
}