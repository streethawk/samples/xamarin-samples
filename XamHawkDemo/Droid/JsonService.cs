﻿using System;
using Android.App;
using Android.Util;
using Android.Content;
using Android.OS;
using Com.Streethawk.Library.Push;
using Android.Support.V4.App;

namespace XamHawkDemo.Droid
{
    /// <summary>
    /// This is a sample started service. When the service is started, it will log a string that details how long 
    /// the service has been running (using Android.Util.Log). This service displays a notification in the notification
    /// tray while the service is active.
    /// </summary>
    [Service]
    class JsonService : Service , ISHObserver
    {
        static readonly string TAG = "streethawk Service";
        bool isStarted;
        private static readonly int JsonNotificationId = 1177;

        public void OnReceiveNonSHPushPayload(Bundle p0)
        {

        }

        public void OnReceivePushData(PushDataForApplication p0)
        {

        }

        public void OnReceiveResult(PushDataForApplication p0, int p1)
        {

        }

        public void ShNotifyAppPage(string p0)
        {

        }

        public void ShReceivedRawJSON(string p0, string p1, string p2)
        {
            // Build the notification:
            Notification.Builder builder = new Notification.Builder(this);
            // Implementation of Big text Notification
            Notification.BigTextStyle textStyle = new Notification.BigTextStyle();

            // Title of notification
            textStyle.SetBigContentTitle(p0);

            // Long message of notification usually JSON data 
            string longTextMessage = p2;           
            textStyle.BigText(longTextMessage);

            // Summary or body in the campaign 
            textStyle.SetSummaryText(p1);
             
            builder.SetStyle(textStyle);
            // Title of notification
            builder.SetContentTitle(p0);
            // Enabling Vibration and Sound
            builder.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate);
            builder.SetSmallIcon(Resource.Drawable.icon); 

            // Finally, publish the notification:
            NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(JsonNotificationId, builder.Build());

        }


        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (isStarted)
            {
                Log.Info(TAG, "OnStartCommand: This service has already been started.");
            }
            else
            {
                Log.Info(TAG, "OnStartCommand: The service is starting.");
                isStarted = true;
            }

            // This tells Android to restart the service if it is killed to reclaim resources.
            return StartCommandResult.Sticky;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            Log.Info(TAG, "OnCreate: the service is initializing.");
            
            Push.GetInstance(this).RegisterSHObserver(this);
        }


        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder that would
            // allow access to the GetFormattedStamp() method.
            return null;
        }

    
    }
}